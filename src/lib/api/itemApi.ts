import { User } from "oidc-client-ts";
import apiClient from "./util/apiClient.ts";

export async function getItems(
  user: User,
  friendId?: string,
): Promise<ItemsDto> {
  const path = `/users/${friendId ? friendId : user.profile.sub}/items`;
  return apiClient
    .get(path, user)
    .catch((reason) => {
      console.error("Failed to get items from API", reason);
    })
    .then((response) => {
      if (response) {
        return response.json() as Promise<ItemsDto>;
      }
      console.error("Got empty response when trying to get items.");
      throw new Error();
    });
}

export async function postItem(
  user: User,
  item: ItemRequestDto,
): Promise<ItemDto> {
  return apiClient
    .post(`/users/${user.profile.sub}/items`, item, user)
    .catch((reason) => {
      console.error("Failed to get items from API", reason);
    })
    .then((response) => {
      if (response) {
        return response.json() as Promise<ItemDto>;
      }
      console.error("Got empty response when trying to get items.");
      throw new Error();
    });
}

export async function deleteItem(
  user: User,
  item: ItemDto,
): Promise<Response | void> {
  return apiClient
    .delete(`/users/${user.profile.sub}/items/${item.id}`, user)
    .catch((reason) => {
      console.error("Failed to delete item via API", reason);
    });
}

export interface ItemRequestDto {
  name: string;
  description: string;
  image: string;
}

export interface ItemDto {
  id: string;
  name: string;
  description: string;
  image: string;
  owner: string;
}

export interface ItemsDto {
  items: ItemDto[];
}
