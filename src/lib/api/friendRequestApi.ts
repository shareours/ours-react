import { User } from "oidc-client-ts";
import apiClient from "./util/apiClient.ts";
import {ApiException, ErrorDto} from "./util/apiException.ts";

export async function postFriendRequest(
  user: User,
  friendRequest: FriendRequestCreationDto,
): Promise<FriendRequestDto> {
  return apiClient
    .post(`/users/${user.profile.sub}/friendrequests`, friendRequest, user)
    .then(async (response) => {
      if (response && response.ok) {
        return await response.json() as FriendRequestDto;
      }
      if (response) {
        const error = await response.json() as ErrorDto
        throw new ApiException(error);
      }
      console.error("Got empty response when trying to create friendrequest");
      throw new Error();
    });
}

export async function getFriendRequests(
  user: User,
): Promise<FriendRequestsDto> {
  return apiClient
    .get(`/users/${user.profile.sub}/friendrequests`, user)
    .then((response) => {
      if (response && response.ok) {
        return response.json() as Promise<FriendRequestsDto>;
      }
      console.error(
        "Got error response when trying to get friendrequests",
        response,
      );
      throw new Error();
    });
}

export async function putFriendRequest(
  user: User,
  friendRequestDto: FriendRequestDto,
  updateFriendRequestDto: UpdateFriendRequestDto,
): Promise<void> {
  return apiClient
    .put(
      `/users/${friendRequestDto.creator}/friendrequests/${friendRequestDto.id}`,
      updateFriendRequestDto,
      user,
    )
    .then((response) => {
      if (response && response.ok) {
        return;
      }
      console.error(
        "Got error response when trying to get friendrequests",
        response,
      );
      throw new Error();
    });
}

export interface FriendRequestCreationDto {
  friendId: string;
}

export interface FriendRequestDto {
  id: string;
  creator: string;
  type: "SEND" | "RECEIVE";
  creatorFirstname: string;
  creatorLastname: string;
}

export interface FriendRequestsDto {
  friendRequests: FriendRequestDto[];
}

export interface UpdateFriendRequestDto {
  type: FriendRequestUpdateType;
}

export enum FriendRequestUpdateType {
  ACCEPT,
  DECLINE,
}
