import { User } from "oidc-client-ts";
import apiClient from "./util/apiClient.ts";

export async function postImage(
  user: User | null | undefined,
  image: Blob,
): Promise<ImageResponseDto> {
  if (!user) {
    throw new Error();
  }
  const formData = new FormData();
  formData.append("image", image);
  return apiClient
    .postFormData(`/images/user/${user.profile.sub}`, formData, user)
    .catch((reason) => {
      console.error("Failed to get items from API", reason);
    })
    .then((response) => {
      if (response) {
        return response.json() as Promise<ImageResponseDto>;
      }
      console.error("Got empty response when trying to get items.");
      throw new Error();
    });
}

export interface ImageResponseDto {
  id: string;
}
