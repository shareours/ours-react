import { User } from "oidc-client-ts";
import apiClient from "./util/apiClient.ts";

export async function updateUser(user: User): Promise<void> {
  const response = apiClient.put("/users/" + user.profile.sub, undefined, user);
  response.catch((reason) => {
    console.error("Failed to update user in API", reason);
  });
}
