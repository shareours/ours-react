import config from "../../../config.json";
import { User } from "oidc-client-ts";

class ApiClient {
  private async request(
    method: "GET" | "POST" | "PUT" | "DELETE",
    path: string,
    user: User,
    body?: string | FormData | Blob,
    headers?: Record<string, string>,
  ) {
    const defaultHeaders = {
      Authorization: `Bearer ${user.access_token}`,
    };

    let requestOptions: RequestInit = {
      method: method,
      headers: {
        ...defaultHeaders,
        ...headers,
      },
    };
    if (body) {
      requestOptions = {
        ...requestOptions,
        body,
      };
    }
    console.log(requestOptions);
    return fetch(config.api.baseUrl + path, requestOptions).catch((error) => {
      console.log("API Request failed: " + JSON.stringify(error));
    });
  }

  public async put(
    path: string,
    data: object | undefined = undefined,
    user: User,
  ) {
    return this.request("PUT", path, user, JSON.stringify(data), {
      "Content-Type": "application/json",
    });
  }

  public async post(
    path: string,
    data: object | undefined = undefined,
    user: User,
  ) {
    return this.request("POST", path, user, JSON.stringify(data), {
      "Content-Type": "application/json",
    });
  }

  public async postFormData(path: string, formData: FormData, user: User) {
    return this.request("POST", path, user, formData);
  }

  public async get(path: string, user: User) {
    return this.request("GET", path, user);
  }

  public async delete(path: string, user: User) {
    return this.request("DELETE", path, user);
  }
}

export default new ApiClient();
