export class ApiException extends Error {
  errorDto: ErrorDto;

  constructor(errorDto: ErrorDto) {
    super();
    this.errorDto = errorDto;
  }
}

export interface ErrorDto {
  status: number;
  code: string;
  message: string;
  details: Map<string, string>;
}
