import { User } from "oidc-client-ts";
import apiClient from "./util/apiClient.ts";

export async function getFriendships(
  user: User,
): Promise<FriendshipsResponseDto> {
  return apiClient
    .get(`/users/${user.profile.sub}/friendships`, user)
    .then((response) => {
      if (response && response.ok) {
        return response.json() as Promise<FriendshipsResponseDto>;
      }
      console.error(
        "Got error response when trying to get friendships",
        response,
      );
      throw new Error();
    });
}

export async function deleteFriendship(
  user: User,
  friendId: string,
): Promise<void> {
  return apiClient
    .delete(`/users/${user.profile.sub}/friendships/${friendId}`, user)
    .then((response) => {
      if (response && response.ok) {
        return;
      }
      console.error(
        "Got error response when trying to delete friendship",
        response,
      );
      throw new Error();
    });
}

export interface FriendshipsResponseDto {
  friendships: FriendshipDto[];
}

export interface FriendshipDto {
  friendId: string;
  firstname: string;
  lastname: string;
}
