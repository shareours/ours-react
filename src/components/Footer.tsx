import { NavLink } from "react-router-dom";

function Footer() {
  return (
    <footer className="border-t border-slate-200 p-6 mt-8">
      <div className="mx-auto w-full max-w-screen-xl md:flex md:items-center md:justify-between">
        <span className="text-sm text-gray-800 sm:text-center dark:text-gray-400">
          Ours - Share because you care
        </span>
        <ul className="mt-3 flex flex-wrap items-center text-sm font-medium text-gray-800 sm:mt-0">
          <li>
            <NavLink to={"/"} className="me-4 hover:underline md:me-6">
              About
            </NavLink>
          </li>
          <li>
            <a
              href="https://gitlab.com/shareours/ours/-/issues/new"
              className="me-4 hover:underline md:me-6"
            >
              Create an issue
            </a>
          </li>
          <li>
            <a
              href="https://gitlab.com/shareours"
              className="me-4 hover:underline md:me-6"
            >
              Gitlab
            </a>
          </li>
        </ul>
      </div>
    </footer>
  );
}

export default Footer;
