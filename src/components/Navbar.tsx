import ours from "../assets/ours.svg";
import React, { useState } from "react";
import { useAuth } from "react-oidc-context";
import { NavLink } from "react-router-dom";
import OursButton from "./util/OursButton.tsx";

function Navbar() {
  const [showMenu, setShowMenu] = useState(true);
  const auth = useAuth();

  function getDisplayClass() {
    if (showMenu) {
      return "block";
    }
    return "hidden";
  }

  const linkClasses =
    "block mt-4 lg:inline-block lg:mt-0 hover:text-gray-500 mr-4 " +
    getDisplayClass();

  function login() {
    auth.signinRedirect();
  }

  function logout() {
    auth.signoutRedirect();
  }

  function getLoginLogoutButton(): React.JSX.Element {
    if (auth.isAuthenticated) {
      return buildButton("Logout", auth.isLoading, logout);
    }
    return buildButton("Login", auth.isLoading, login);
  }

  function buildButton(text: string, loading: boolean, onClick: () => void) {
    return (
      <OursButton
        loading={loading}
        onClick={onClick}
        className={`${getDisplayClass()} mt-4 lg:mt-0`}
      >
        {text}
      </OursButton>
    );
  }

  return (
    <nav className="flex flex-wrap items-center justify-between rounded border-b border-slate-200 p-6">
      <NavLink to="" className="mr-6 flex flex-shrink-0 items-center">
        <img
          src={ours}
          className="mr-2 h-8 w-8 fill-current"
          width="54"
          height="54"
          alt="icon"
        ></img>
        <span className="text-xl font-semibold tracking-tight">Ours</span>
      </NavLink>
      <div className="block lg:hidden">
        <button
          onClick={() => setShowMenu(!showMenu)}
          className="flex items-center rounded border border-slate-200 px-3 py-2 hover:border-gray-500 hover:text-gray-500"
        >
          <svg
            className="h-3 w-3 fill-current"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>
      <div className="block w-full flex-grow lg:flex lg:w-auto lg:items-center">
        <div className="text-base lg:flex-grow">
          <NavLink to="" className={linkClasses}>
            Home
          </NavLink>
          {auth.isAuthenticated && (
            <NavLink to="items" className={linkClasses}>
              My Items
            </NavLink>
          )}
          {auth.isAuthenticated && (
            <NavLink to="friends" className={linkClasses}>
              Friends
            </NavLink>
          )}
          <NavLink to="#responsive-header" className={linkClasses}>
            Sign Up
          </NavLink>
        </div>
        <div>{getLoginLogoutButton()}</div>
      </div>
    </nav>
  );
}

export default Navbar;
