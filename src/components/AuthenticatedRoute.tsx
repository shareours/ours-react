import {withAuthenticationRequired} from "react-oidc-context";
import {Outlet} from "react-router-dom";


function AuthenticatedRoute() {

    return withAuthenticationRequired(() => <Outlet/>, {
        OnRedirecting: () => <div>Redirecting to Login...</div>
    })({})
}

export default AuthenticatedRoute;