import { JSX, MouseEventHandler } from "react";

interface ButtonProps {
  children: string;
  onClick?: MouseEventHandler<HTMLButtonElement> | undefined;
  className?: string;
  loading?: boolean;
  type?: "submit" | "reset" | "button" | undefined;
}

function OursButton(props: ButtonProps): JSX.Element {
  const defaultClasses =
    "inline-block text-sm px-4 py-2 leading-none border rounded border-slate-600 hover:border-transparent hover:text-white hover:bg-black block min-w-fit";
  return (
    <button
      disabled={props.loading}
      type={props.type}
      onClick={props.onClick}
      className={`${defaultClasses} ${props.className} ${props.loading ? "animate-pulse" : ""}`}
    >
      {props.loading ? "Loading..." : props.children}
    </button>
  );
}

export default OursButton;
