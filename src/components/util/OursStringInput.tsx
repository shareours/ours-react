import React, { forwardRef } from "react";

interface InputProps {
  title: string;
  className?: string;
}

const OursStringInput = forwardRef(
  (props: InputProps, ref: React.ForwardedRef<HTMLInputElement>) => {
    const defaultClasses =
      "block w-full rounded-lg border p-2.5 text-sm text-gray-900 focus:border-slate-200 focus:outline-slate-200";
    return (
      <div>
        <label className="mb-2 mt-4 block font-medium">{props.title}</label>
        <input
          type="text"
          className={`${defaultClasses} ${props.className}`}
          placeholder={props.title}
          required
          ref={ref}
        />
      </div>
    );
  },
);
export default OursStringInput;
