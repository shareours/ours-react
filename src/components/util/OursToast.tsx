import { ToastType, useToastState } from "../../states/errorState.ts";

function OursToast() {
  const toastState = useToastState();

  function reset() {
    toastState.setError("");
  }

  function getIconColorClasses() {
    switch (toastState.type) {
      case ToastType.ERROR:
        return "bg-red-100 text-red-500";
      case ToastType.SUCCESS:
        return "text-green-500 bg-green-100";
    }
  }

  function getIconScreenReaderDescription() {
    switch (toastState.type) {
      case ToastType.ERROR:
        return "Attention Icon";
      case ToastType.SUCCESS:
        return "Check Icon";
    }
  }

  function getIcon() {
    switch (toastState.type) {
      case ToastType.ERROR:
        return (
          <svg
            className="h-5 w-5"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="currentColor"
            viewBox="0 0 20 20"
          >
            <path d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5ZM10 15a1 1 0 1 1 0-2 1 1 0 0 1 0 2Zm1-4a1 1 0 0 1-2 0V6a1 1 0 0 1 2 0v5Z" />
          </svg>
        );
      case ToastType.SUCCESS:
        return (
          <svg
            className="h-5 w-5"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="currentColor"
            viewBox="0 0 20 20"
          >
            <path d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5Zm3.707 8.207-4 4a1 1 0 0 1-1.414 0l-2-2a1 1 0 0 1 1.414-1.414L9 10.586l3.293-3.293a1 1 0 0 1 1.414 1.414Z" />
          </svg>
        );
    }
  }

  if (toastState.message.length == 0) {
    return;
  }
  return (
    <div
      className="fixed bottom-32 flex h-fit w-full max-w-xs items-center rounded-lg border bg-white p-4 md:right-5 md:top-5"
      role="alert"
    >
      <div
        className={`inline-flex h-8 w-8 flex-shrink-0 items-center justify-center rounded-lg ${getIconColorClasses()}`}
      >
        {getIcon()}
        <span className="sr-only">{getIconScreenReaderDescription()}</span>
      </div>
      <div className="ms-3 max-w-52 break-words text-sm font-normal">
        {toastState.message}
      </div>
      <button
        type="button"
        className="ms-auto inline-flex h-8 w-8 items-center justify-center rounded-lg p-1.5 hover:bg-black hover:text-white"
        aria-label="Close"
        onClick={reset}
      >
        <span className="sr-only">Close</span>
        <svg
          className="h-3 w-3"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 14 14"
        >
          <path
            stroke="currentColor"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
          />
        </svg>
      </button>
    </div>
  );
}

export default OursToast;
