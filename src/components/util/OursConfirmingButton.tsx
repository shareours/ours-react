import { JSX, useState } from "react";
import OursButton from "./OursButton.tsx";

interface ButtonProps {
  children: string;
  onClick: () => void;
  className?: string;
  loading?: boolean;
}

function OursConfirmingButton(props: ButtonProps): JSX.Element {
  const [confirmationState, setConfirmationState] = useState(false);

  function handleConfirmation() {
    return () => {
      props.onClick();
      setConfirmationState(false);
    };
  }

  return (
    <div className={"m-2 flex"}>
      {confirmationState && (
        <div className={"flex flex-col gap-2 md:flex-row"}>
          <OursButton onClick={() => setConfirmationState(false)}>
            Cancel
          </OursButton>
          <OursButton loading={props.loading} onClick={handleConfirmation()}>
            Confirm
          </OursButton>
        </div>
      )}
      {!confirmationState && (
        <OursButton onClick={() => setConfirmationState(true)}>
          {props.children}
        </OursButton>
      )}
    </div>
  );
}

export default OursConfirmingButton;
