import React, { useMemo, useState } from "react";
import OursButton from "../util/OursButton.tsx";
import {
  FriendRequestDto,
  FriendRequestUpdateType,
} from "../../lib/api/friendRequestApi.ts";
import { FriendshipDto } from "../../lib/api/friendshipApi.ts";
import { Link } from "react-router-dom";
import OursConfirmingButton from "../util/OursConfirmingButton.tsx";

interface UserCardProps {
  className?: string;
  loading?: boolean;
  type?: "submit" | "reset" | "button" | undefined;
  index?: React.Key;
  isRequest?: boolean;
  updateFriendRequestFn?: (
    friendRequest: FriendRequestDto,
    type: FriendRequestUpdateType,
  ) => Promise<void>;
  friendRequest?: FriendRequestDto;
  deleteFriendshipFn?: (friend: FriendshipDto) => Promise<void>;
  friendship?: FriendshipDto;
}

function UserCard(props: UserCardProps) {
  const [loading, setLoading] = useState<
    FriendRequestUpdateType | undefined | "delete"
  >(undefined);

  const shownName = useMemo(() => {
    if (props.isRequest) {
      return `${props.friendRequest!.creatorFirstname} ${props.friendRequest!.creatorLastname}`;
    }
    return `${props.friendship!.firstname} ${props.friendship!.lastname}`;
  }, [props.friendRequest, props.friendship, props.isRequest]);

  function acceptFriendRequest() {
    setLoading(FriendRequestUpdateType.ACCEPT);
    props.updateFriendRequestFn!(
      props.friendRequest!,
      FriendRequestUpdateType.ACCEPT,
    ).finally(() => setLoading(undefined));
  }

  function declineFriendRequest() {
    setLoading(FriendRequestUpdateType.DECLINE);
    props.updateFriendRequestFn!(
      props.friendRequest!,
      FriendRequestUpdateType.DECLINE,
    ).finally(() => setLoading(undefined));
  }

  function deleteFriendship() {
    setLoading("delete");
    props.deleteFriendshipFn!(props.friendship!).finally(() =>
      setLoading(undefined),
    );
  }

  function getProfilePictureAndUserName() {
    return (
      <div className={"flex w-full flex-grow items-center"}>
        <svg
          viewBox="0 0 36 36"
          fill="none"
          role="img"
          xmlns="http://www.w3.org/2000/svg"
          width="40"
          height="40"
          className={"me-2 block min-w-fit"}
        >
          <mask
            id=":ri:"
            maskUnits="userSpaceOnUse"
            x="0"
            y="0"
            width="36"
            height="36"
          >
            <rect width="36" height="36" rx="72" fill="#FFFFFF"></rect>
          </mask>
          <g mask="url(#:ri:)">
            <rect width="36" height="36" fill="#49007e"></rect>
            <rect
              x="0"
              y="0"
              width="36"
              height="36"
              transform="translate(-4 8) rotate(168 18 18) scale(1)"
              fill="#ff7d10"
              rx="36"
            ></rect>
            <g transform="translate(0 4) rotate(-8 18 18)">
              <path d="M13,19 a1,0.75 0 0,0 10,0" fill="#000000"></path>
              <rect
                x="11"
                y="14"
                width="1.5"
                height="2"
                rx="1"
                stroke="none"
                fill="#000000"
              ></rect>
              <rect
                x="23"
                y="14"
                width="1.5"
                height="2"
                rx="1"
                stroke="none"
                fill="#000000"
              ></rect>
            </g>
          </g>
        </svg>
        <h5 className="min-w-px break-words text-lg">{shownName}</h5>
      </div>
    );
  }

  return (
    <div
      key={props.index}
      className={`flex flex-col rounded border border-gray-200 bg-white p-3 shadow ${props.className}`}
    >
      {props.isRequest && (
        <>
          {getProfilePictureAndUserName()}
          <div className={"mt-1 flex w-full"}>
            <OursButton
              className={"me-4 h-fit"}
              onClick={acceptFriendRequest}
              loading={loading == FriendRequestUpdateType.ACCEPT}
            >
              Accept
            </OursButton>
            <OursButton
              className={"h-fit"}
              onClick={declineFriendRequest}
              loading={loading == FriendRequestUpdateType.DECLINE}
            >
              Decline
            </OursButton>
          </div>
        </>
      )}
      {!props.isRequest && (
        <>
          <Link to={`/friends/${props.friendship!.friendId}/items`}>
            {getProfilePictureAndUserName()}
          </Link>
          <div className={"mt-1 flex w-full"}>
            <OursConfirmingButton
              className={"mr-2 mt-2 h-fit"}
              onClick={deleteFriendship}
              loading={loading == "delete"}
            >
              Delete
            </OursConfirmingButton>
          </div>
        </>
      )}
    </div>
  );
}

export default UserCard;
