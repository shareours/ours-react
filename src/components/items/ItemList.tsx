import { useEffect, useState } from "react";
import { useAuth } from "react-oidc-context";
import { deleteItem, getItems, ItemDto } from "../../lib/api/itemApi.ts";
import { useNavigate } from "react-router-dom";
import config from "../../config.json";
import { User } from "oidc-client-ts";
import OursConfirmingButton from "../util/OursConfirmingButton.tsx";

interface ItemListProps {
  userId?: string;
  showDeleteButton?: boolean;
}

function ItemList(props: ItemListProps) {
  const [items, setItems] = useState<ItemDto[]>([]);
  const auth = useAuth();
  const navigate = useNavigate();
  const [user, setUser] = useState<User | null>(null);

  useEffect(() => {
    const user = auth.user;
    if (user) {
      setUser(user);
      getItems(user, props.userId).then((items) => {
        setItems(items.items);
      });
    } else {
      if (!auth.isLoading) {
        navigate("/");
      }
    }
  }, [auth, navigate, props.userId]);

  if (items.length == 0) {
    return (
      <div>
        Seems like you have not added any items yet. Start by pressing the
        button above to add one!
      </div>
    );
  }

  function handleDeleteItem(item: ItemDto): void {
    if (!user) {
      return;
    }
    deleteItem(user, item).then(() =>
      setItems((prevState) => prevState.filter((value) => value.id != item.id)),
    );
  }

  return (
    <div className={"flex flex-col justify-center"}>
      {items.map((item, index) => (
        <div
          key={index}
          className={
            "my-2 flex flex-grow flex-row gap-2 space-x-4 rounded border border-slate-200"
          }
        >
          <img
            className={"size-32 min-w-32 rounded object-fill lg:size-44"}
            src={`${config.api.baseUrl}/images/${item.image}`}
            alt={"image"}
          />
          <div className={"flex min-w-px flex-grow flex-col space-y-2"}>
            <div className={"flex flex-row justify-between"}>
              <h1 className={"mt-2 break-words font-bold"}>{item.name}</h1>
              {props.showDeleteButton && (
                <OursConfirmingButton
                  className={"float-right mr-2 mt-2 h-8"}
                  onClick={() => handleDeleteItem(item)}
                >
                  Delete
                </OursConfirmingButton>
              )}
            </div>
            <p className={"break-words"}>{item.description}</p>
          </div>
        </div>
      ))}
    </div>
  );
}

export default ItemList;
