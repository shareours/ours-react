import ItemList from "../../components/items/ItemList.tsx";
import OursButton from "../../components/util/OursButton.tsx";
import { useNavigate } from "react-router-dom";

function ItemsPage() {
  const navigate = useNavigate();

  return (
    <div className={"flex justify-center"}>
      <div className={"my-4 flex w-11/12 min-w-px flex-col lg:w-2/3"}>
        <OursButton
          onClick={() => navigate("add")}
          className={"w my-4 lg:w-24"}
        >
          Add Item
        </OursButton>
        <ItemList showDeleteButton={true} />
      </div>
    </div>
  );
}

export default ItemsPage;
