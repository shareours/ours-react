import ItemList from "../../components/items/ItemList.tsx";
import { useParams } from "react-router-dom";
import { useFriendsState } from "../../states/friendsState.ts";

function FriendsItemsPage() {
  const friendships = useFriendsState.getState().friendships;
  const { friendId } = useParams();

  const friend = friendships.find(
    (friendship) => friendship.friendId == friendId,
  );

  const firstName = friend?.firstname || "";
  const lastName = friend?.lastname || "";

  return (
    <div className={"flex justify-center"}>
      <div className={"my-4 flex w-11/12 min-w-px flex-col lg:w-2/3"}>
        <h1 className={"mt-4 break-words text-xl font-bold"}>
          Items of {firstName} {lastName}
        </h1>
        <ItemList userId={friendId} showDeleteButton={false} />
      </div>
    </div>
  );
}

export default FriendsItemsPage;
