import React, { useRef, useState } from "react";
import { postItem } from "../../lib/api/itemApi.ts";
import { useAuth } from "react-oidc-context";
import { postImage } from "../../lib/api/imageApi.ts";
import OursButton from "../../components/util/OursButton.tsx";
import { useNavigate } from "react-router-dom";
import ReactCropper, { ReactCropperElement } from "react-cropper";
import "cropperjs/dist/cropper.css";
import OursStringInput from "../../components/util/OursStringInput.tsx";
import { useToastState } from "../../states/errorState.ts";

function AddItemPage() {
  const navigate = useNavigate();
  const auth = useAuth();
  const name = useRef<HTMLInputElement>(null);
  const description = useRef<HTMLInputElement>(null);
  const fileInput = useRef<HTMLInputElement>(null);
  const cropperRef = useRef<ReactCropperElement>(null);
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState<string | undefined>("");
  const errorState = useToastState();

  function postNewItem(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    setLoading(true);
    const user = auth.user;
    if (!image || !user || !cropperRef.current) {
      setLoading(false);
      errorState.setError(
        "Something went wrong while creating the item. Please try again later.",
      );
      return;
    }
    cropperRef.current.cropper.getCroppedCanvas().toBlob((blob) => {
      if (!blob) {
        setLoading(false);
        errorState.setError(
          "Something went wrong while creating the item. Please try again later.",
        );
        return;
      }
      postImage(auth.user, blob)
        .then((imageResponseDto) => {
          postItem(user, {
            name: name.current?.value || "",
            description: description.current?.value || "",
            image: imageResponseDto.id,
          }).then(() => navigate("/items"));
        })
        .catch((error) => {
          console.error(`Got error while trying to create item: ${error}`);
          setLoading(false);
          errorState.setError(
            "Something went wrong while creating the item. Please try again later.",
          );
        });
    });
  }

  function onFileInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    event.preventDefault();
    const files = event.target.files;
    if (files == null) {
      return;
    }
    const reader = new FileReader();
    reader.onload = () => {
      setImage(reader.result as string);
    };
    reader.readAsDataURL(files[0]);
  }

  return (
    <form onSubmit={postNewItem}>
      <div className={"flex justify-center"}>
        <div className={"my-4 flex w-11/12 flex-col lg:w-2/3"}>
          {image && (
            <div className={"w-fit max-w-full"}>
              <ReactCropper
                style={{ maxHeight: "24rem"}}
                aspectRatio={1}
                viewMode={2}
                guides={false}
                ref={cropperRef}
                src={image}
              />
            </div>
          )}
          <label className="mb-2 mt-4 block font-medium">Image</label>
          <div className={"flex flex-row"}>
            <input
              required
              type="file"
              className={
                "block w-full rounded-lg border text-sm text-gray-900 file:mr-4 file:cursor-pointer file:rounded-lg file:border file:bg-white file:px-4 file:py-2 file:hover:border-transparent file:hover:bg-black file:hover:text-white"
              }
              accept={"image/png"}
              onChange={onFileInputChange}
              ref={fileInput}
            />
            {image && (
              <OursButton
                className={"ml-4"}
                type={"button"}
                onClick={() => {
                  const current = fileInput.current;
                  if (current) {
                    current.value = "";
                  }
                  setImage(undefined);
                }}
              >
                Remove
              </OursButton>
            )}
          </div>
          <OursStringInput title={"Name"} ref={name} />
          <OursStringInput title={"Description"} ref={description} />
          <OursButton
            type={"submit"}
            className={"mt-4 lg:ml-auto lg:w-20"}
            loading={loading}
          >
            Submit
          </OursButton>
        </div>
      </div>
    </form>
  );
}

export default AddItemPage;
