function Home() {
  return (
    <div>
      <h1 className={"m-4 text-center text-xl font-bold"}>
        Ours - Share because you care.
      </h1>
      <p className={"my-4 text-center"}>
        Ours is a platform where you can sign up and register items which you
        want to share with friends or in communities.
      </p>
    </div>
  );
}

export default Home;
