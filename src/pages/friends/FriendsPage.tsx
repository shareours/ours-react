import OursButton from "../../components/util/OursButton.tsx";
import React, { useEffect, useRef, useState } from "react";
import UserCard from "../../components/users/UserCard.tsx";
import {
  FriendRequestDto,
  FriendRequestUpdateType,
  getFriendRequests,
  postFriendRequest,
  putFriendRequest,
} from "../../lib/api/friendRequestApi.ts";
import { useAuth } from "react-oidc-context";
import {
  deleteFriendship,
  FriendshipDto,
} from "../../lib/api/friendshipApi.ts";
import { useToastState } from "../../states/errorState.ts";
import { ApiException } from "../../lib/api/util/apiException.ts";
import { updateFriends, useFriendsState } from "../../states/friendsState.ts";

function FriendsPage() {
  const auth = useAuth();
  const userId = useRef<HTMLInputElement>(null);
  const friendRequestForm = useRef<HTMLFormElement>(null);
  const [sendingFriendRequest, setSendingFriendRequest] = useState(false);
  const [friendRequests, setFriendRequests] = useState<FriendRequestDto[]>([]);
  const { friendships } = useFriendsState();
  const { setError, setSuccess } = useToastState();

  useEffect(() => {
    if (!auth.user) {
      return;
    }
    getFriendRequests(auth.user)
      .then((friendRequestsDto) => {
        setFriendRequests(friendRequestsDto.friendRequests);
      })
      .catch((error) => {
        setError("Requesting friend requests failed.");
        console.error(error);
      });
    updateFriends(auth.user).catch((error) => {
      setError("Failed to update friends");
      console.error(error);
    });
  }, [auth.user, setError]);

  function createFriendRequest(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    if (!auth.user || !userId.current) {
      return;
    }
    if (auth.user.profile.sub == userId.current.value) {
      setError("Sharing with yourself is boring. Go find some real friends!");
      return;
    }
    setSendingFriendRequest(true);
    postFriendRequest(auth.user, { friendId: userId.current.value })
      .then(() => {
        setSendingFriendRequest(false);
        friendRequestForm.current?.reset();
      })
      .catch((e) => {
        if (e instanceof ApiException) {
          if (e.errorDto.status == 409) {
            setError("Friend request or friendship already exists.");
          }
        } else {
          setError("Sending friend request failed.");
        }
        setSendingFriendRequest(false);
      });
  }

  async function updateFriendRequest(
    friendRequestDto: FriendRequestDto,
    type: FriendRequestUpdateType,
  ): Promise<void> {
    if (!auth.user) {
      return Promise.reject();
    }
    try {
      await putFriendRequest(auth.user, friendRequestDto, { type: type });
      setFriendRequests(
        friendRequests.filter((value) => value.id != friendRequestDto.id),
      );
      if (type === FriendRequestUpdateType.ACCEPT) {
        await updateFriends(auth.user);
      }
    } catch (error) {
      setError("Updating friend request failed.");
      console.error(error);
    }
  }

  async function deleteFriend(friendshipDto: FriendshipDto): Promise<void> {
    if (!auth.user) {
      return Promise.reject();
    }
    try {
      await deleteFriendship(auth.user, friendshipDto.friendId);
      await updateFriends(auth.user);
    } catch (error) {
      setError("Deleting friend failed.");
      console.error(error);
    }
  }

  function buildFriendRequestsIfExists() {
    if (friendRequests.length == 0) return;
    return (
      <>
        <h1 className={"mx-1 my-3 text-xl font-bold"}>
          Friend Requests ({friendRequests.length})
        </h1>
        <div
          className={
            "mx-1 grid grid-cols-1 gap-2 md:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4"
          }
        >
          {friendRequests.map((friendRequest, index) => (
            <UserCard
              key={index}
              isRequest={true}
              updateFriendRequestFn={updateFriendRequest}
              friendRequest={friendRequest}
            />
          ))}
        </div>
      </>
    );
  }

  function copyMyId() {
    if (!auth.user) {
      setError("Not logged in.");
      return;
    }
    navigator.clipboard
      .writeText(auth.user?.profile.sub)
      .then(() => {
        setSuccess("Copied your ID.");
      })
      .catch(() => {
        setError("Failed to write to clipboard.");
      });
  }

  return (
    <div className={"flex justify-center"}>
      <div className={"my-4 flex w-11/12 min-w-px flex-col lg:w-4/5"}>
        <form
          className={"mb-4 flex flex-wrap"}
          onSubmit={createFriendRequest}
          ref={friendRequestForm}
        >
          <label className="mx-1 w-full py-3 lg:w-fit">
            Enter a user ID to add a friend:
          </label>
          <input
            type="text"
            className={
              "w-full rounded-lg border p-2 focus:border-slate-200 focus:outline-slate-200 lg:w-2/5"
            }
            placeholder={"User ID"}
            required
            ref={userId}
          />
          <OursButton
            type={"submit"}
            className={"my-2 w-full px-2 lg:mx-2 lg:w-24 lg:px-0"}
            loading={sendingFriendRequest}
          >
            Add
          </OursButton>
          <OursButton
            type={"button"}
            className={"my-2 w-full px-2 lg:mx-2 lg:w-24 lg:px-4"}
            onClick={copyMyId}
          >
            Copy My Id
          </OursButton>
        </form>
        {buildFriendRequestsIfExists()}
        <h1 className={"mx-1 my-3 text-xl font-bold"}>
          Your Friends ({friendships.length})
        </h1>
        <div
          className={
            "grid grid-cols-1 gap-2 md:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4"
          }
        >
          {friendships.map((friend, index) => (
            <UserCard
              friendship={friend}
              key={index}
              deleteFriendshipFn={deleteFriend}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

export default FriendsPage;
