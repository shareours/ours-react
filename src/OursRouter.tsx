import { BrowserRouter, Route, Routes } from "react-router-dom";
import AuthenticatedRoute from "./components/AuthenticatedRoute.tsx";
import ItemsPage from "./pages/items/ItemsPage.tsx";
import Navbar from "./components/Navbar.tsx";
import AddItemPage from "./pages/items/AddItemPage.tsx";
import Home from "./pages/Home.tsx";
import Footer from "./components/Footer.tsx";
import OursToast from "./components/util/OursToast.tsx";
import FriendsPage from "./pages/friends/FriendsPage.tsx";
import FriendsItemsPage from "./pages/items/FriendsItemsPage.tsx";

function oursRouter() {
  return (
    <BrowserRouter>
      <main className="flex min-h-screen flex-col justify-between">
        <section className={"flex-grow"}>
          <Navbar />
          <div className={"flex justify-center"}>
            <OursToast />
          </div>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route element={<AuthenticatedRoute />}>
              <Route path="/items">
                <Route index element={<ItemsPage />} />
                <Route path="/items/add" element={<AddItemPage />} />
              </Route>
              <Route path="/friends">
                <Route index element={<FriendsPage />} />
                <Route
                  path="/friends/:friendId/items"
                  element={<FriendsItemsPage />}
                />
              </Route>
            </Route>
          </Routes>
        </section>
        <Footer />
      </main>
    </BrowserRouter>
  );
}

export default oursRouter;
