import { create } from "zustand";

export enum ToastType {
  ERROR,
  SUCCESS,
}

interface ToastState {
  message: string;
  type: ToastType;
  setError: (message: string) => void;
  setSuccess: (message: string) => void;
}

export const useToastState = create<ToastState>()((set) => ({
  message: "",
  type: ToastType.ERROR,
  setError: (message) =>
    set(() => ({ message: message, type: ToastType.ERROR })),
  setSuccess: (message) =>
    set(() => ({ message: message, type: ToastType.SUCCESS })),
}));
