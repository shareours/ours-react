import { create } from "zustand";
import { FriendshipDto, getFriendships } from "../lib/api/friendshipApi.ts";
import { User } from "oidc-client-ts";
import { devtools, persist } from "zustand/middleware";

interface FriendsState {
  friendships: FriendshipDto[];
  setFriendships: (friendships: FriendshipDto[]) => void;
}

export const useFriendsState = create<FriendsState>()(
  devtools(
    persist(
      (set) => ({
        friendships: [],
        setFriendships: (friends: FriendshipDto[]) =>
          set({ friendships: [...friends] }),
      }),
      { name: "friendsState" },
    ),
  ),
);

export const updateFriends = async (user: User) => {
  const friendshipsResponseDto = await getFriendships(user);
  useFriendsState.getState().setFriendships(friendshipsResponseDto.friendships);
};
