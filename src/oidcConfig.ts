import { AuthProviderProps } from "react-oidc-context";
import { User, WebStorageStateStore } from "oidc-client-ts";
import { updateUser } from "./lib/api/userApi.ts";
import config from "./config.json";
import { updateFriends } from "./states/friendsState.ts";

const onSigninCallback = (user: void | User): void => {
  if (user) {
    updateUser(user).catch((reason) => console.error(reason));
    updateFriends(user).catch((reason) => console.error(reason));
  }
  window.history.replaceState({}, document.title, window.location.pathname);
};

const oidcConfig: AuthProviderProps = {
  authority: config.keycloak.baseUrl + "/realms/" + config.keycloak.realm,
  client_id: config.keycloak.client,
  redirect_uri: window.location.href,
  onSigninCallback: onSigninCallback,
  userStore: new WebStorageStateStore({ store: window.localStorage }),
  post_logout_redirect_uri: config.baseUrl,
};

export default oidcConfig;
