import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { AuthProvider } from "react-oidc-context";
import OursRouter from "./OursRouter.tsx";
import oidcConfig from "./oidcConfig.ts";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <AuthProvider {...oidcConfig}>
      <OursRouter />
    </AuthProvider>
  </React.StrictMode>,
);
